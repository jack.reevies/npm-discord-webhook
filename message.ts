import { EMBED_MAX_COMBINED, MESSAGE_MAX_EMBEDS } from "./constants"
import { Embed, IEmbed } from "./embed"

export interface IMessage {
  content?: string
  username?: string
  avatarUrl?: string
  tts?: boolean
  embeds?: Embed[]
}

export class Message implements IMessage {
  content?: string
  username?: string
  avatarUrl?: string
  tts?: boolean
  embeds?: Embed[]

  constructor(data: IMessage) {
    for (const prop in data) {
      this[prop as keyof (Message)] = data[prop as keyof (IMessage)] as never
    }
  }

  addEmbed(embed: Embed) {
    if (!this.embeds) {
      this.embeds = []
    }
    this.embeds?.push(embed)
    return this
  }

  setContent(content: string){
    this.content = content
    return this
  }

  setUsername(username: string) {
    this.username = username
    return this
  }

  setAvatarUrl(url: string) {
    this.avatarUrl = url
    return this
  }

  setTts(tts: boolean) {
    this.tts = tts
    return this
  }

  validate() {
    if (!this.content && !this.embeds?.length) {
      throw new Error('Webhook should have either a content or at least one embed')
    }
    if (this.content && this.embeds?.length) {
      throw new Error('Webhook cannot have both a content and an array of embeds')
    }
    if (this.embeds?.length && this.embeds.length > MESSAGE_MAX_EMBEDS) {
      throw new Error(`Webhook cannot have more than ${MESSAGE_MAX_EMBEDS} embeds`)
    }

    if (!this.embeds) return this

    let totalChars = 0

    for (const embed of this.embeds) {
      embed.validate()

      totalChars += (embed.title?.length || 0)
        + (embed.description?.length || 0)
        + (embed.footer?.text.length || 0)
        + (embed.author?.name.length || 0)

      if (!embed.fields) continue

      for (const field of embed.fields) {
        totalChars += field.name.length + field.value.length
      }
    }

    if (totalChars > EMBED_MAX_COMBINED) {
      throw new Error(`Combined sum of characters in title, description, field.name, field.value, footer.text, and author.name fields in all embeds cannot exceed ${EMBED_MAX_COMBINED}`)
    }

    return this
  }
}