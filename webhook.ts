import fetch from "node-fetch"
import { IEmbed } from "./embed"
import { IMessage, Message } from "./message"

export class Webhook {
  url: string

  constructor(url: string) {
    this.url = url
  }

  createNewMessage(data: IMessage) {
    return new Message(data)
  }

  async sendSimpleMessage(content: string) {
    return this.sendMessage(new Message({ content }))
  }

  async sendMessage(message: Message) {
    message.validate()
    const discordJson = convertToDiscordTypes(message)
    const body = JSON.stringify(discordJson)
    const res = await fetch(this.url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body
    })

    if (!res.ok) {
      const json: { message: string, code: number } = await res.json()
      throw new Error(json.message || 'Discord returned a non-descript error')
    }
    return res.ok
  }
}

function convertToDiscordTypes(msg: IMessage) {
  const json = convertToDiscordJson(msg) as IMessage
  if (json.embeds) {
    for (const embed of json.embeds) {
      for (const prop in embed) {
        if (typeof (embed[prop as keyof (IEmbed)]) === 'object' && !Array.isArray(embed[prop as keyof (IEmbed)])) {
          embed[prop as keyof (IEmbed)] = convertToDiscordJson(embed[prop as keyof (IEmbed)] as any) as never
        }
      }
    }
  }
  return json
}

function convertToDiscordJson(obj: { proxyIconUrl?: string, iconUrl?: string, avatarUrl?: string, proxyUrl?: string }) {
  const tor = {
    ...obj
  } as any

  if (tor.avatarUrl) {
    tor.avatar_url = tor.avatarUrl
    delete tor.avatarUrl
  }

  if (tor.iconUrl) {
    tor.icon_url = tor.iconUrl
    delete tor.iconUrl
  }

  if (tor.proxyIconUrl) {
    tor.proxy_icon_url = tor.proxyIconUrl
    delete tor.proxyIconUrl
  }

  if (tor.proxyUrl) {
    tor.proxy_url = tor.proxyUrl
    delete tor.proxyUrl
  }
  return tor
}