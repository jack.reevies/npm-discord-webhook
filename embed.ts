import { AUTHOR_MAX_NAME, EMBED_MAX_COMBINED, EMBED_MAX_DESCRIPTION, EMBED_MAX_FIELDS, EMBED_MAX_TITLE, FIELD_MAX_NAME, FIELD_MAX_VALUE, FOOTER_MAX_TEXT } from "./constants"

export interface Author {
  name: string
  url?: string
  iconUrl?: string
  proxyIconUrl?: string
}

export interface Footer {
  text: string
  iconUrl?: string
  proxyIconUrl?: string
}

export interface Thumbnail {
  url: string
  proxyUrl?: string
  height?: number
  width?: number
}

export type Nullable<T> = T | undefined | null
export interface Video extends Thumbnail { }
export interface Image extends Thumbnail { }

export interface Field {
  name: string
  value: string
  inline?: boolean
}

export interface IEmbed {
  title?: string
  description?: string
  url?: string
  timestamp?: string
  color?: number
  footer?: Footer
  image?: Image
  thumbnail?: Thumbnail
  video?: Video
  author?: Author
  fields?: Field[]
}

export class Embed implements IEmbed {
  title?: string
  description?: string
  url?: string
  timestamp?: string
  color: number = 0
  footer?: Footer
  image?: Image
  thumbnail?: Thumbnail
  video?: Video
  author?: Author
  fields?: Field[]

  constructor(data?: IEmbed) {
    if (!data) return
    for (const prop in data) {
      this[prop as keyof (Embed)] = data[prop as keyof (IEmbed)] as never
    }
  }

  setColorHex(hex: string) {
    this.color = parseInt(hex.replace('#', ''), 16)
    return this
  }

  setTitle(title: string) {
    this.title = title
    return this
  }

  setDescription() {
    this.description = this.description
    return this
  }

  setUrl() {
    this.url = this.url
    return this
  }

  setTimestamp(timestamp: string) {
    this.timestamp = timestamp
    return this
  }

  setFooter(text: string, iconUrl?: string, proxyIconUrl?: string) {
    this.footer = { text, iconUrl, proxyIconUrl }
    return this
  }

  setImage(url: string, height?: number, width?: number, proxyUrl?: string) {
    this.image = { url, height, width, proxyUrl }
    return this
  }

  setThumbnail(url: string, height?: number, width?: number, proxyUrl?: string) {
    this.thumbnail = { url, height, width, proxyUrl }
    return this
  }

  setVideo(url: string, height?: number, width?: number, proxyUrl?: string) {
    this.video = { url, height, width, proxyUrl }
    return this
  }

  setAuthor(name: string, url?: string, iconUrl?: string, proxyIconUrl?: string) {
    this.author = { name, url, iconUrl, proxyIconUrl }
    return this
  }

  addField(name: string, value: string, inline?: boolean) {
    if (!this.fields) {
      this.fields = []
    }
    this.fields.push({ name, value, inline })
    return this
  }

  addFields(fields: Field[]) {
    if (!this.fields) {
      this.fields = []
    }
    fields.forEach(o => this.fields?.push(o))
    return this
  }

  validate() {
    if (this.title && this.title?.length > EMBED_MAX_TITLE) {
      throw new Error(`Embed Title must not be more than ${EMBED_MAX_TITLE} characters`)
    }
    if (this.description && this.description.length > EMBED_MAX_DESCRIPTION) {
      throw new Error(`Embed Description must not be more than ${EMBED_MAX_DESCRIPTION} characters`)
    }
    if (this.footer && this.footer.text.length > FOOTER_MAX_TEXT) {
      throw new Error(`Embed Footer Text must not be more than ${FOOTER_MAX_TEXT} characters`)
    }
    if (this.author && this.author.name.length > AUTHOR_MAX_NAME) {
      throw new Error(`Embed Author Name must not be more than ${AUTHOR_MAX_NAME} characters`)
    }

    if (!this.fields) return this

    if (this.fields.length > 25) {
      throw new Error(`Embed must not exceed ${EMBED_MAX_FIELDS} fields`)
    }

    let totalChars = (this.title?.length || 0)
      + (this.description?.length || 0)
      + (this.footer?.text.length || 0)
      + (this.author?.name.length || 0)

    for (const field of this.fields) {
      validateField(field)
      totalChars += field.name.length + field.value.length
    }

    if (totalChars > EMBED_MAX_COMBINED) {
      throw new Error(`Combined sum of characters in title, description, field.name, field.value, footer.text, and author.name fields cannot exceed ${EMBED_MAX_COMBINED}`)
    }
  }
}

function validateField(field: Field) {
  if (!field.name || !field.name.length) {
    throw new Error(`Field Name must be present and contain more than 0 characters`)
  }
  if (!field.value || !field.value.length) {
    throw new Error(`Field Value must be present and contain more than 0 characters`)
  }
  if (field.name.length > FIELD_MAX_NAME) {
    throw new Error(`Field Text must not be more than ${FIELD_MAX_NAME} characters`)
  }
  if (field.value.length > FIELD_MAX_VALUE) {
    throw new Error(`Field Text must not be more than ${FIELD_MAX_VALUE} characters`)
  }
}